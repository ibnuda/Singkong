{-# LANGUAGE RecordWildCards #-}
{-|
Module Lib
-}
module Lib where

import qualified Data.ByteString.Lazy    as BL
import           Data.Csv
import qualified Data.List               as L
import qualified Data.Text.Lazy          as TL
import qualified Data.Text.Lazy.Encoding as TLE
import qualified Data.Vector             as V

import           Lib.Internal
import           Lib.Prelude
import           Lib.Types

-- | Entry point.
-- 1. Read the `CallerAndAgentData.csv`.
-- 2. If the result doesn't have a proper format, exit.
-- 3. Else, normalize and then answer the questions.
someFunc :: IO ()
someFunc = do
  csvData <- BL.readFile "CallerandAgentData.csv"
  case decodeByName csvData of
    Left _ ->
      putStrLn
        ("Alas, poor Yorick. I knew him, Horatio. A fellow of infinite jest, of most excellent fancy." :: Text)
    Right (_, v) -> do
      let denormalized = V.toList . denormalize $ v
      writeDenormalizedCallLogs denormalized
      number02 denormalized
      number03 denormalized
      number04 denormalized
      number05 denormalized
      number06 denormalized
      number07 denormalized
      number08 denormalized
      number09 denormalized
      number10 denormalized

-- | Write the denormalized data (which has been parsed) to a file.
writeDenormalizedCallLogs :: [CallLog] -> IO ()
writeDenormalizedCallLogs cls = do
  BL.writeFile
    "denormalized.csv"
    (TLE.encodeUtf8 $
     TL.intercalate "\n" $ L.map (TL.fromStrict . showCallLog) cls)

-- | 1. Sum the `CallLog`s by their `callDate`
-- 2. Find the maximum value of `CallLog` based on the `completed` field.
-- 3. Print the `callDate` and `completed` field.
number02 :: MonadIO m => [CallLog] -> m ()
number02 cls = do
  putStr ("no 02:\t" :: Text)
  let mostByCompleted =
        maximumBy (\a b -> compare (completed a) (completed b)) $
        sumByCallDate cls
  putStrLn $
    ((show $ callDate mostByCompleted) <> " " <>
     (show $ completed mostByCompleted) :: Text)

-- | 1. Group the `CallLog`s by 24s.
-- 2. Sum the `CallLog`s.
-- 3. Find the maximum value of `CallLog` based on the `completed` field.
-- 4. Print the `completed` field.
number03 :: MonadIO m => [CallLog] -> m ()
number03 cls = do
  putStr ("no 03:\t" :: Text)
  print $
    completed $
    maximumBy (\a b -> compare (completed a) (completed b)) $
    L.map sumCallLog $ groupOf24s cls

-- | 1. Sum the `CallLog`s by their `callTime`
-- 2. Find the maximum value of `CallLog` based on the `request` field.
-- 3. Print the `callTime` field.
number04 :: MonadIO m => [CallLog] -> m ()
number04 cls = do
  putStr ("no 04:\t" :: Text)
  print $
    callTime $
    maximumBy (\a b -> compare (request a) (request b)) $
    sumByCallTime cls

-- | 1. Sum `CallLog`s which is in the range of `weekend`.
-- 2. Sum all `CallLog`s.
-- 3. Result of step number 1 / result of step number 2.
number05 :: MonadIO m => [CallLog] -> m ()
number05 cls = do
  putStr ("no 05:\t" :: Text)
  let zeroesWeekend = zeroes . sumCallLog $ filterInWeekend cls
      zeroesAll = zeroes . sumCallLog $ cls
  print $
    ((fromIntegral zeroesWeekend) * 100 / (fromIntegral zeroesAll) :: Double)

-- | 1. Group the `CallLog`s by 8s.
-- 2. Sum each group into a single `CallLog`.
-- 3. Transform it into a `Vector`.
-- 4. Find the maximum by comparing the `request` field.
number06 :: MonadIO m => [CallLog] -> m ()
number06 cls = do
  putStr ("no 06:\t" :: Text)
  let busiest8Hour =
        V.maximumBy (\a b -> compare (request a) (request b)) .
        V.fromList . map sumCallLog $
        groupOf8s cls
  putStrLn $
    (show $ callDate busiest8Hour :: Text) <> " " <>
    (show $ callTime busiest8Hour :: Text) <>
    " " <>
    (show $ request busiest8Hour :: Text)

-- | 1. Take the list of increased demand and call agents based on the comparison between `CallLog n` and `CallLog ( n - 1 )`
-- 2. If the result of the previous step is the same list then it's `True`, else it's `False`.
number07 :: MonadIO m => [CallLog] -> m ()
number07 cls = do
  putStr ("no 07:\t" :: Text)
  let listIncReq = listOfIncreasedDemand cls
      listIncCAg = listOfIncreasedCallAgent cls
  if (listIncReq == listIncCAg)
    then putStrLn ("True" :: Text)
    else putStrLn ("False" :: Text)

-- | 1. Group the `CallLog`s into groups of 72s.
-- 2. Sum the group of `CallLog`s.
-- 3. Transform it into `Vector`s.
-- 4. Find the maximum based on the ratio between `zeroes` and `eyeballs`.
number08 :: MonadIO m => [CallLog] -> m ()
number08 cls = do
  putStr ("no 08:\t" :: Text)
  let mostByRatio =
        V.maximumBy
          (\a b ->
             compare
               ((fromIntegral $ zeroes a) / (fromIntegral $ eyeballs a) :: Double)
               ((fromIntegral $ zeroes b) / (fromIntegral $ eyeballs b) :: Double)) .
        V.fromList . map sumCallLog $
        groupOf72s cls
  putStrLn $
    (show $ callDate mostByRatio :: Text) <> " - " <> (show $ callTime mostByRatio :: Text)

-- | 1. Sum the `CallLog`s based on their `callTime`.
-- 2. Transform it into a `Vector`.
-- 3. Find the minimum by comparing the ratio of `completed` and `request` field.
number09 :: MonadIO m => [CallLog] -> m ()
number09 cls = do
  putStr ("no 09:\t" :: Text)
  let ini =
        V.minimumBy
          (\a b ->
             compare
               ((fromIntegral $ completed a) / (fromIntegral $ request a) :: Double)
               ((fromIntegral $ completed b) / (fromIntegral $ request b) :: Double)) .
        V.fromList . sumByCallTime $
        cls
  putStrLn $
    (show $ callTime ini :: Text)

-- | 1. Sum the `CallLog`s based on their `callTime`.
-- 2. Transform it to a `Vector`.
-- 3. Find the maximum by comparing the ratio of `completed` and `request` field.
number10 :: MonadIO m => [CallLog] -> m ()
number10 cls = do
  putStr ("no 10:\t" :: Text)
  let ini =
        V.maximumBy
          (\a b ->
             compare
               ((fromIntegral $ completed a) / (fromIntegral $ request a) :: Double)
               ((fromIntegral $ completed b) / (fromIntegral $ request b) :: Double)) .
        V.fromList . sumByCallTime $
        cls
  putStrLn $
    (show $ callTime ini :: Text)

{-# LANGUAGE RecordWildCards #-}
{-|
Module Lib.Types
-}
module Lib.Types
  ( CallLog(..)
  ) where

import           Control.Applicative
import           Data.ByteString.Char8 (unpack)
import           Data.Csv
import           Data.Time             (Day, defaultTimeLocale,
                                        parseTimeOrError)

import           Lib.Prelude

-- | The main type of this application
data CallLog = CallLog
  { callDate  :: Maybe Day
  -- ^ Date of the log.
  , callTime  :: !Int
  -- ^ Time of the log.
  , eyeballs  :: !Int
  -- ^ Ryeballs.
  , zeroes    :: !Int
  -- ^ Zeroes.
  , completed :: !Int
  -- ^ Completed.
  , request   :: !Int
  -- ^ Requests.
  , agent     :: !Int
  -- ^ Agents.
  } deriving (Eq)

-- | Comparing two `CallLog` should be the same as comparing the two
-- dates of them.
instance Ord CallLog where
  compare (CallLog a _ _ _ _ _ _) (CallLog b _ _ _ _ _ _) = compare a b

-- | Just standard instance of `FromField`.
instance FromField Day where
  parseField = pure . dayParser

-- | Just standard instance of `FromNamedRecord`.
instance FromNamedRecord CallLog where
  parseNamedRecord m =
    CallLog
    <$> m .: "Date"
    <*> m .: "Time (Local)"
    <*> m .: "Eyeballs"
    <*> m .: "Zeroes"
    <*> m .: "Completed Calls"
    <*> m .: "Requests"
    <*> m .: "Unique Agents"

-- | We just have to make the parser to be able accept an empty field.
dayParser :: ByteString -> Day
dayParser = parseTimeOrError True defaultTimeLocale "%d-%b-%y" . unpack

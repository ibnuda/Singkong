{-# LANGUAGE RecordWildCards #-}
{-|
Module Lib.Internal
-}
module Lib.Internal where

import           Control.Monad
import qualified Data.List                   as L
import           Data.Maybe
import qualified Data.Text                   as T
import           Data.Time.Calendar.WeekDate
import qualified Data.Vector                 as V

import           Lib.Prelude
import           Lib.Types

-- | Te ease printing.
showCallLog :: CallLog -> Text
showCallLog CallLog {..} =
  (T.pack $ show callDate) <>
  "\t" <>
  (T.intercalate "\t\t" $
  map (show) [callTime, eyeballs, zeroes, completed, request, agent])

-- | When summing two `CallLog`s, we just have to add the fields.
addCallLog :: CallLog -> CallLog -> CallLog
addCallLog c1 c2
  | isNothing $ callDate c1 =
    CallLog
      (callDate c2)
      (callTime c2)
      (eyeballs c1 + eyeballs c2)
      (zeroes c1 + zeroes c2)
      (completed c1 + completed c2)
      (request c1 + request c2)
      (agent c1 + agent c2)
  -- shortcut
  | otherwise =
    CallLog
      (callDate c1)
      (callTime c1)
      (eyeballs c1 + eyeballs c2)
      (zeroes c1 + zeroes c2)
      (completed c1 + completed c2)
      (request c1 + request c2)
      (agent c1 + agent c2)

-- | An empty `CallLog`.
emptyCallLog :: CallLog
emptyCallLog = CallLog Nothing 0 0 0 0 0 0

-- | Fold a list of `CallLogs` into an `emptyCallLog`.
sumCallLog :: [CallLog] -> CallLog
sumCallLog = L.foldl addCallLog emptyCallLog

-- | Group a list of `CallLog`s to groups of the same `callDate`.
groupByCallDate :: [CallLog] -> [[CallLog]]
groupByCallDate = (L.groupBy (\a b -> callDate a == callDate b))

-- | Sum a list of `CallLog` by summing them based on the `callDate`
sumByCallDate :: [CallLog] -> [CallLog]
sumByCallDate = L.map sumCallLog . groupByCallDate

-- | Group a list of `CallLog`s to groups of the same `callTime`.
groupByCallTime :: [CallLog] -> [[CallLog]]
groupByCallTime = (L.groupBy (\a b -> callTime a == callTime b))

-- | Sum a list of `CallLog` by summing them based on the `callTime`
sumByCallTime :: [CallLog] -> [CallLog]
sumByCallTime = L.map sumCallLog . groupByCallTime

-- | Filter a list of `CallLogs` so it is only has weekends.
filterInWeekend :: [CallLog] -> [CallLog]
filterInWeekend = L.filter isWeekend

-- | Decide whether a `CallLog` is in the weekend.
-- The conditions are:
-- 1. Is in Friday and in afternoon.
-- 2. Or is in saturday.
-- 3. Or is in sunday and in nighttime.
isWeekend :: CallLog -> Bool
isWeekend CallLog {..} =
  let (_, _, day) = toWeekDate . fromJust $ callDate
      isFriday = (== 5)
      isSaturday = (== 6)
      isSunday = (== 7)
      isNight = (< 3)
      isAfternoon = (>= 15)
  in (isFriday day && isAfternoon callTime) ||
     isSaturday day || (isSunday day && isNight callTime)

-- | Group the `CallLogs` into groups of 8 consecutive `CallLog`s.
groupOf8s :: [CallLog] -> [[CallLog]]
groupOf8s = groupByNumber 8

-- | Group the `CallLogs` into groups of 24 consecutive `CallLog`s.
groupOf24s :: [CallLog] -> [[CallLog]]
groupOf24s = groupByNumber 24

-- | Group the `CallLogs` into groups of 72 consecutive `CallLog`s.
groupOf72s :: [CallLog] -> [[CallLog]]
groupOf72s = groupByNumber 72

-- | Generic function to group `CallLog`s.
groupByNumber :: Int -> [CallLog] -> [[CallLog]]
groupByNumber number cls =
  let h = L.take number cls
  in if L.length h < number
       then []
       else h : (groupByNumber number $ L.drop 1 cls)

-- | Filter a list of `CallLog`s based on the comparasion between a record to its successor.
-- If the value is lesser, then we can consider it as an increase.
-- Thus we include it into the list of the result.
listOfIncreasedDemand :: [CallLog] -> [CallLog]
listOfIncreasedDemand []             = []
listOfIncreasedDemand (_:[])         = []
listOfIncreasedDemand (cla:clb:rest) =
  if (request cla) < (request clb)
    then clb : (listOfIncreasedDemand rest)
    else listOfIncreasedDemand rest

-- | Filter a list of `CallLog`s based on the comparasion between a record to its successor.
-- If the value is lesser, then we can consider it as an increase.
-- Thus we include it into the list of the result.
listOfIncreasedCallAgent :: [CallLog] -> [CallLog]
listOfIncreasedCallAgent []             = []
listOfIncreasedCallAgent (_:[])         = []
listOfIncreasedCallAgent (cla:clb:rest) =
  if (agent cla) < (agent clb)
    then cla : (listOfIncreasedCallAgent (clb : rest))
    else listOfIncreasedCallAgent (clb : rest)

-- | Unneeded.
showIncreases :: [CallLog] -> [CallLog] -> IO ()
showIncreases clsa clsb = do
  putStrLn ("date a\t\ttime a\tto ->\tdate b\t\ttime b \tdata\tfrom\tto\tinc/dec?" :: Text)
  sequence_ $ L.zipWith printTwo clsa clsb
  where
    printTwo :: CallLog -> CallLog -> IO ()
    printTwo cla clb = do
      putStrLn $
        (show $ callDate cla) <> " " <> (show $ callTime cla) <>
        ("\t->\t" :: Text) <>
        (show $ callDate clb) <>
        " " <>
        (show $ callTime clb) <>
        "\tREQ\t" <>
        (show $ request cla) <>
        "\t" <>
        (show $ request clb) <>
        (if (request cla < request clb)
           then "\tINC"
           else "\tDEC") <>
        "\n\t\t\t\t\t\t\tAGN\t" <>
        (show $ agent cla) <>
        "\t" <>
        (show $ agent clb) <>
        (if (agent cla < agent clb)
           then "\tINC"
           else "\tDEC") <>
        "\n"

-- | Just to fill the empty field of the input so it has value.
denormalize :: V.Vector CallLog -> V.Vector CallLog
denormalize cls = V.scanl1 copyDate cls
  where
    copyDate :: CallLog -> CallLog -> CallLog
    copyDate firstLog secondLog
      | isJust $ callDate secondLog = secondLog
      | otherwise = secondLog {callDate = callDate firstLog}

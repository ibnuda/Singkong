# Singkong

1. Install https://github.com/commercialhaskell/stack.
2. Change working directory to `Singkong` directory.
3. `stack setup`.
4. `stack build`.
5. `stack exec Singkong-exe`
